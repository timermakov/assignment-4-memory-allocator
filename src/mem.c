#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

// Инициализация блока
static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

// Получить актуальный размер региона
static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

// Смапить страницы
void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {

  // Выясняем настоящий размер региона по вместимости блока
  const block_size actual_size = (block_size) {
    region_actual_size(size_from_capacity((block_capacity) {query}).bytes)
  };

  // Смапить страницы по адресу региона
  void* region_addr = map_pages(addr, actual_size.bytes, MAP_FIXED_NOREPLACE);

  // Если не получилось
  if (region_addr == MAP_FAILED) {
    region_addr = map_pages(addr, actual_size.bytes, false);

    if (region_addr == MAP_FAILED) {
      return REGION_INVALID;
    }
  }

  // Создаём структуру
  struct region region = (struct region) { .addr = region_addr, .size = actual_size.bytes, .extends = region_addr == addr };

  // Инициализация блока
  block_init(region_addr, (block_size) {actual_size.bytes}, NULL);

  // Возвращаем регион
  return region;
}

static void* block_after( struct block_header const* block )         ;

// Инициализация кучи
void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  // Если блок маленький, то и разделять не надо
  if (!block_splittable(block, query)) {
    return false;
  }

  // Если блок большой, делим
  void *generated_block_addr = (void*)((uint8_t*) block + offsetof(struct block_header, contents) + query);
  block_size generated_block_size = (block_size) {block->capacity.bytes - query};
  block_init(generated_block_addr, generated_block_size, block->next);
  block->next = generated_block_addr;
  block->capacity = (block_capacity) { query };
  
  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

// Возвращает булево значение, могут ли 2 блока быть соединены
static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

// Соединить со следующим блоком
static bool try_merge_with_next( struct block_header* block ) {
  if (block->next == NULL || !mergeable(block, block->next)) return false;
  else {
  block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
  block->next = block->next->next;
  return true;
  }
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last ( struct block_header* restrict block, size_t sz )    {
  // Если переданный в параметре блок ошибочный
  if (!block) {
    return (struct block_search_result) {
      .type = BSR_CORRUPTED,
      .block = NULL
    };
  }
  
  // Проверяем, если блок ненулевой, то пытаемся объединить
  while (block) {

    while (try_merge_with_next(block)) {}
  
    // Возвращаем подходящий (достаточно большой и свободен)
    if(block_is_big_enough(sz, block) && block->is_free) {
      return (struct block_search_result) { 
        BSR_FOUND_GOOD_BLOCK, 
        block
      };
    }

    // Перейти к следующему, если он не последний
    if (block->next == NULL) {
      break;
    }
    block = block->next;
  }
 
  // Дойдя до конца, не нашли
  return (struct block_search_result) { BSR_REACHED_END_NOT_FOUND, block};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  size_t memalloc_from_block_query = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result result = find_good_or_last(block, memalloc_from_block_query);
  if (!result.type) {
    split_if_too_big(result.block, memalloc_from_block_query);
    result.block->is_free = false;
  }
  return result;
}

// Наращиваем кучу
static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  // Если последний пустой, возвращаем NULL
  if (!last) {
    return NULL;
  }
  // Выделяем память для региона
  struct region region = alloc_region((void *)block_after(last), size_max(query, BLOCK_MIN_CAPACITY));
  
  // Если не получилось, возвращаем NULL
  if (region_is_invalid(&region)) {
    return NULL;
  }

  // Инициализация блока
  block_init(region.addr, (block_size) { region.size }, NULL);

  // Присоединить новый блок к цепочке блоков
  last->next = region.addr;

  // Если следующий блок свободен, объединить, иначе вернуть указатель на следующий регион
  if (last->is_free && try_merge_with_next(last)) return last;
  return last->next; 
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  // Проверка на то, что запрашиваемый размер не меньше минимального, если меньше, то увеличиваем до минимального
  query = size_max(query, BLOCK_MIN_CAPACITY);
  // Первое приближение
  struct block_search_result bsr = try_memalloc_existing(query, heap_start);

  // Второе приближение
  if (bsr.type == BSR_REACHED_END_NOT_FOUND) {
    bsr = try_memalloc_existing(size_max(query, BLOCK_MIN_CAPACITY), 
            grow_heap(bsr.block, size_max(query, BLOCK_MIN_CAPACITY)));
  }
  // Не нашли подходящий блок
  if (bsr.type != BSR_FOUND_GOOD_BLOCK) {
    return NULL;
  }
  // Если нашли подходящий блок по результатам двух приближений
  // Пометить блок как занятый 
  bsr.block->is_free = false;
  return bsr.block;
  
}

// Использовать эту функцию для вызова нашего аллокатора памяти
void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

// Возвращает заголовок блока
static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

// Освобождение памяти
void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  // Соединяем со следующими блоками
  while(try_merge_with_next(header));
}
