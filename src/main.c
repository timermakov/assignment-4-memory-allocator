#define _DEFAULT_SOURCE
#define HEAP_SIZE 1000

#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define INITIAL_HEAP_SIZE 10240

// Инициализация кучи
static void* test_heap_init() {
    printf("%s","Heap initializing started\n");
    void* heap = heap_init(INITIAL_HEAP_SIZE);
    if (!heap) 
        err("Cannot initialize heap");
    printf("%s", "Heap initialized\n\n");
    return heap;
}

// Возвращает указатель на блок по указателю на данные
static inline struct block_header* get_block_by_allocated_data(void* data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}

// Обычное успешное выделение памяти
void test_normal_memory_allocation (struct block_header* first_block) {
    printf("%s", "Test 1 started\n");
    const size_t test_size = 512;
    void* data1 = _malloc(test_size);
    if (!data1)
        err("Test 1 failed: _malloc returned NULL\n");
    debug_heap(stdout, first_block);
    if (first_block->is_free)
        err("Test 1 failed: allocated_block isn't free\n");
    if(first_block->capacity.bytes != test_size)
        err("Test 1 failed: allocated_block size isn't correct\n");
    printf("%s", "Test 1 passed successfully\n\n");
    _free(data1);
}

// Освобождение одного блока из нескольких выделенных
void test_free_block(struct block_header *first_block) {
    printf("%s", "Test 2 started\n");
    void* data1 = _malloc(512);
    void* data2 = _malloc(700);
    if (!data1 || !data2)
        err("Test 2 failed: _malloc returned NULL\n");
    _free(data1);
    debug_heap(stdout, first_block);
    struct block_header* data_block1 = get_block_by_allocated_data(data1);
    struct block_header* data_block2 = get_block_by_allocated_data(data2);
    if (!data_block1->is_free)
        err("Test 2 failed: free block is taken\n");
    if (data_block2->is_free)
        err("Test 2 failed: taken block is free\n");
    printf("%s", "Test 2 passed successfully\n\n");
    _free(data1);
    _free(data2);
}

// Освобождение двух блоков из нескольких выделенных.
void test_free_two_blocks(struct block_header* first_block) {
    printf("%s", "Test 3 started\n");
    const size_t s1 = 512; 
    const size_t s2 = 1024; 
    const size_t s3 = 2048;
    void* data1 = _malloc(s1);
    void* data2 = _malloc(s2);
    void* data3 = _malloc(s3);
    if (!data1 || !data2 || !data3)
        err("Test 3 failed: _malloc returned NULL\n");
    _free(data2);
    _free(data1);
    debug_heap(stdout, first_block);
    struct block_header *data_block1 = get_block_by_allocated_data(data1), *data_block3 = get_block_by_allocated_data(data3);
    if (!data_block1->is_free)
        err("Test 3 failed: free block is taken\n");
    if (data_block3->is_free) 
        err("Test 3 failed: taken block is free\n");
    if (data_block1->capacity.bytes != s1 + s2 + offsetof(struct block_header, contents))
        err("Test 3 failed: two free blocks didn't connect\n");
    printf("%s", "Test 3 passed successfully\n\n");
    _free(data3);
    _free(data2);
    _free(data1);
}

// Память закончилась, новый регион памяти расширяет старый
void test_new_region_after_last(struct block_header* first_block) {
    printf("%s", "Test 4 started\n");
    void* data1 = _malloc(INITIAL_HEAP_SIZE);
    void* data2 = _malloc(INITIAL_HEAP_SIZE + 512);
    void* data3 = _malloc(2048);
    if (!data1 || !data2 || !data3)
        err("Test 4 failed: _malloc returned NULL\n");
    _free(data3);
    _free(data2);
    debug_heap(stdout, first_block);
    struct block_header* data_block1 = get_block_by_allocated_data(data1);
    struct block_header* data_block2 = get_block_by_allocated_data(data2);
    if ((uint8_t *)data_block1->contents + data_block1->capacity.bytes != (uint8_t*) data_block2){
        err("Test 4 failed: new region wasn't created after last\n");
    }
    printf("%s", "Test 4 passed successfully\n\n");
    _free(data3);
    _free(data2);
    _free(data1);
}

// Память закончилась, старый регион памяти не расширить из-за другого
// выделенного диапазона адресов, новый регион выделяется в другом месте
void test_new_region_at_new_place() {
    printf("%s", "Test 5 started\n");
    void* heap = heap_init(HEAP_SIZE);
    void* map = mmap(HEAP_START + 8192, 4096, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
    debug_heap(stdout, heap);
    void* alloc_1 = _malloc(10000);
    if (!alloc_1) {
        err("Test 5 failed: _malloc returned NULL\n");
    }
    debug_heap(stdout, heap);
    _free(alloc_1);
    debug_heap(stdout, heap);
    munmap(map, 4096);
	printf("%s", "Test 5 passed successfully\n\n");
}

// Вызов всех тестов
int main(){
    struct block_header* first_block = (struct block_header*) test_heap_init();
    test_normal_memory_allocation(first_block);
    test_free_block(first_block);
    test_free_two_blocks(first_block);
    test_new_region_after_last(first_block);
    test_new_region_at_new_place();
    printf("%s", "All tests passed successfully\n\n");
    return 0;
}
