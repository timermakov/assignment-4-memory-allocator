# CMake generated Testfile for 
# Source directory: /home/user/PL/lab4/assignment-4-memory-allocator/tester
# Build directory: /home/user/PL/lab4/assignment-4-memory-allocator/src/tester
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test__free "/home/user/PL/lab4/assignment-4-memory-allocator/src/tester/test__free")
set_tests_properties(test__free PROPERTIES  _BACKTRACE_TRIPLES "/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;22;add_test;/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;0;")
add_test(test_alloc_region "/home/user/PL/lab4/assignment-4-memory-allocator/src/tester/test_alloc_region")
set_tests_properties(test_alloc_region PROPERTIES  _BACKTRACE_TRIPLES "/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;22;add_test;/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;0;")
add_test(test_find_good_or_last "/home/user/PL/lab4/assignment-4-memory-allocator/src/tester/test_find_good_or_last")
set_tests_properties(test_find_good_or_last PROPERTIES  _BACKTRACE_TRIPLES "/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;22;add_test;/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;0;")
add_test(test_grow_heap "/home/user/PL/lab4/assignment-4-memory-allocator/src/tester/test_grow_heap")
set_tests_properties(test_grow_heap PROPERTIES  _BACKTRACE_TRIPLES "/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;22;add_test;/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;0;")
add_test(test_heap_init "/home/user/PL/lab4/assignment-4-memory-allocator/src/tester/test_heap_init")
set_tests_properties(test_heap_init PROPERTIES  _BACKTRACE_TRIPLES "/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;22;add_test;/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;0;")
add_test(test_memalloc "/home/user/PL/lab4/assignment-4-memory-allocator/src/tester/test_memalloc")
set_tests_properties(test_memalloc PROPERTIES  _BACKTRACE_TRIPLES "/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;22;add_test;/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;0;")
add_test(test_split_if_too_big "/home/user/PL/lab4/assignment-4-memory-allocator/src/tester/test_split_if_too_big")
set_tests_properties(test_split_if_too_big PROPERTIES  _BACKTRACE_TRIPLES "/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;22;add_test;/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;0;")
add_test(test_try_memalloc_existing "/home/user/PL/lab4/assignment-4-memory-allocator/src/tester/test_try_memalloc_existing")
set_tests_properties(test_try_memalloc_existing PROPERTIES  _BACKTRACE_TRIPLES "/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;22;add_test;/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;0;")
add_test(test_try_merge_with_next "/home/user/PL/lab4/assignment-4-memory-allocator/src/tester/test_try_merge_with_next")
set_tests_properties(test_try_merge_with_next PROPERTIES  _BACKTRACE_TRIPLES "/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;22;add_test;/home/user/PL/lab4/assignment-4-memory-allocator/tester/CMakeLists.txt;0;")
