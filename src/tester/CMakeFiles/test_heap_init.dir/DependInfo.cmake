# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/user/PL/lab4/assignment-4-memory-allocator/tester/tests/heap_init.c" "/home/user/PL/lab4/assignment-4-memory-allocator/src/tester/CMakeFiles/test_heap_init.dir/tests/heap_init.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "DEBUG"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../tester/src"
  "."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/user/PL/lab4/assignment-4-memory-allocator/src/src/CMakeFiles/memalloc.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
